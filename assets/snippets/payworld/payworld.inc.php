<?php

    require_once 
        MODX_BASE_PATH."assets/snippets/payworld/config.php";

    $dbprefix = $modx->db->config['table_prefix'];
    $mod_table = $dbprefix.'manager_shopkeeper';

    switch ($_REQUEST['action'])
    {
        case 'fail':
        case 'success':
            $output = $_REQUEST['result_message'];
            break;

        case 'callback':
            if (isset($_REQUEST['transaction_id']) 
                && isset($_REQUEST['order_id']) 
                && isset($_REQUEST['order_total'])
                && isset($_REQUEST['payer_email']) 
                && isset($_REQUEST['seller_name']) 
                && isset($_REQUEST['shop_id'])
                && isset($_REQUEST['hash']))
            {
                $hash = md5(
                      $_REQUEST['order_id'] 
                    . $_REQUEST['order_total'] 
                    . $_REQUEST['transaction_id'] 
                    . $_REQUEST['payer_email'] 
                    . $_REQUEST['seller_name'] 
                    . $_REQUEST['shop_id'] 
                    . SECRET_CODE
                );

                if ($_REQUEST['hash'] == $hash)
                {
                    $order = $modx->db->getRow(
                        $modx->db->select(
                             " id, short_txt, content, allowed, addit,"
                            ." price, currency,"
                            ." DATE_FORMAT(date,'%d.%m.%Y %k:%i')"
                            ." AS date, status, email, phone, payment,"
                            ." tracking_num,  userid", 
                            $mod_table, 
                            "id = " . $_REQUEST['order_id'], 
                            "", 
                            ""
                        )
                    );

                    if ($order)
                    {
                        $update_arr = array(
                            'status' => 6
                        );
                        $change_status = $modx->db->update(
                            $update_arr, 
                            $mod_table, 
                            'id = ' . $_REQUEST['order_id']
                        );
                        $modx->invokeEvent(
                            'OnSHKChangeStatus',
                            array(
                                'order_id'=>$_REQUEST['order_id'],
                                'status'=>6
                            )
                        );
                        echo 'Success';
                    }
                    else
                    {
                        notify_by_email(
                            EMAIL_ON_ERROR, 
                            'Ошибка. Заказ не найден.', 
                            'Поступила оплата заказа №' 
                                . $_REQUEST['order_id'] 
                                . ' на сумму '
                                . $_REQUEST['order_total']
                                . ' руб. '
                                . ' Заказ с таким номером не найден!'
                        );

                        header('HTTP/1.1 500 Internal Server Error');
                        die('No order found');
                    }
                }
                else
                {
                    notify_by_email(
                        EMAIL_ON_ERROR, 
                        'Ошибка. Попытка подмены данных об оплате.', 
                        'Поступили данные об оплате с неверным хэшем:'
                            . ' заказ №'
                            . $_REQUEST['order_id'] 
                            . ' на сумму '
                            . $_REQUEST['order_total']
                            . ' руб. '
                    );

                    header('HTTP/1.1 500 Internal Server Error');
                    die('Wrong hash');
                }
            }
            else
            {
                header('HTTP/1.1 500 Internal Server Error');
                die('Not all parameters');
            }
            break;

        case 'payment':    
        default:
            if ($_SESSION['shk_payment_method'] == 'payworld')
            {
                $order_id = $_SESSION['shk_order_id'];
                $amount = 
                    number_format(
                        $_SESSION['shk_order_price'], 2, '.', ''
                    );

                $change_status = $modx->db->update(
                    array('status' => 2), 
                    $mod_table, 
                    "id = $order_id"
                );
                $modx->invokeEvent(
                    'OnSHKChangeStatus',
                    array('order_id'=>$order_id,'status'=>2)
                );

                // Формируем детали заказа
                $order = $modx->db->getRow(
                    $modx->db->select(
                        "id, content", 
                        $mod_table, 
                        "id = " . $order_id, 
                        "", 
                        ""
                    )
                );
                $purchases = unserialize($order['content']);
                // Считаем цены товаров
                foreach ($purchases as $key => $val)
                {
                    $price = $val['tv']['price'];
                    if (isset($val['tv_add']) 
                        && is_array($val['tv_add']))
                    {
                        foreach ($val['tv_add'] as $k => $v)
                        {
                            $idx = preg_replace('/^shk_/', '', $k);
                            
                            $re = "/".preg_quote($v)."==([0-9,\.]*)/";
                            preg_match($re, $val['tv'][$idx], $matches);
                            $price += $matches[1];
                            
                        }
                    }
                    $order_details .= $val[3] 
                        . ' (' . $price .'x' . $val[1] . 'шт.); ';
                }

                $output  = "<form action='".SERVER."' method='post'>";
                $output .= "<input type='hidden' name='order_id' "
                    ."value='".$order_id."'>";
                $output .= "<input type='hidden' name='order_total' "
                    ."value='".$amount."'>";
                $output .= "<input type='hidden' name='order_details' "
                    ."value='".$order_details."'>";
                $output .= "<input type='hidden' name='seller_name' "
                    ."value='".SELLER_NAME."'>";
                $output .= "<input type='hidden' name='shop_id' "
                    ."value='".SHOP_ID."'>";
                $output .= "<input type='submit' name='submit' "
                    ."value='".BUTTON_TEXT."'>";
            }
            break;
    }

    return $output;



    function notify_by_email($to, $subject, $message)
    {
        if ($to)
        {
            $headers  = "From: \"Payworld Error Notifier\" " 
                . "<no-reply@" . $_SERVER['HTTP_HOST'] . ">\r\n"
                . "MIME-Version: 1.0\r\n"
                . "Content-type: text/plain; charset=utf-8\r\n"
                . "Content-Transfer-Encoding: 8bit";

            mail(
                $to,
                "=?utf-8?B?" . base64_encode($subject) . "?=",
                $message,
                $headers
            );
        }
    }

?>
